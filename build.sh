#!/bin/bash

pandoc slides.md \
-t revealjs -s \
-o index.html \
--citeproc \
--metadata pagetitle="Sigil Séance Slides" \
--bibliography /home/david/Library/Zotero_current.bib \
--csl harvard-cite-them-right-SLIDE.csl \
-c style.css \
-V revealjs-url=https://unpkg.com/reveal.js@4.6.0/ \
-V controls=true \
-V theme=black \
-V transition=none \
-V slideNumber=\"c/t\" \
-V backgroundTransition=none \
-V autoPlayMedia=true