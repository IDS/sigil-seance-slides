

Our project started in 2021 with the first passenger flight of Jeff Bezos' space-tourism rocket _New Shepard_. This moment acted like a science-fictional vignette, highlighting the figure of the space-billionaire as the arch-villain of our era, a cartoonish character hoarding wealth on an incomprehensible scale, lifting fascistic ideology from sci-fi novels, and leaving a trail of death and destruction behind them.

"Leave the billionaires in space" read the headline of an article by Parix Marx (2021) after the New Shepard launch, a sentiment we shared. This paper is about our response to this destructive billionaire figure and the future it promotes. We present *Sigil Séance*, a teleperformance based on chaos magick and sigils, that aims to create a space for thinking critically about the billionaire space race, and to manifest alternatives. As creative practictioners we seek to establish a form of praxis, both reflective and actively engaged in the current manifold crises that are unfolding.

Lucile Olympe Haute is an artist, lecturer in design at Université de Nîmes (FR) and associate researcher at École des arts décoratifs of Paris (FR). 

David Benqué is a designer and researcher based in Cork. I hold a PhD from the Royal College of Art and I now operate independently as The Institute of Diagram Studies.

---

Our focus is the billionaire space race that has been capturing headlines in recent years, involving companies such as Blue Origin (Jeff Bezos), Space X (Elon Musk), or Virgin Galactic (Richard Brandson). 

---

While space tourism is attention grabbing, and indeed triggered our own interest in this area, it is only a small part of much bigger shifts currently at play. By opening space exploration to commercial interests in 2015, the United States unleashed a new wave of entrepreneurs and startup companies all looking to profit.

---

In line with the conference theme, we pay particular attention to the future proposed by space billionaires as a very _radical_ one, in the sense of radicalized and dangerous. We identify at least two vectors of harm in this future: the ecocidal and colonial.

---

Looking to outer space first reveals familiar colonial patterns and operations. Utrata (2023) describes a process of disposession: delineate a territory, thereby turning it into an asset so that it can be claimed as property. 

---

As Smiles (2020) points out, the "myths" of American settler colonialism are alive and well in the current discourse around space. Terms such as 'manifest destiny', 'frontier' and underlying concepts such as 'terra nulius' prop up projects such as the settling of Mars by Elon Musk (2017). 

---

Turning our attention back to Earth orbit reveals a more mundane story, but perhaps more toxic as it is already partially deployed. 
We see an infrastructural expansion and control. Amazon for  example plans to place datacenters in space for more efficient data  streaming via satellites (liu and Kim, 2019). 

---

Space X has already colonised the sky, wrapping the planet in a tightly coreographed layer of Starlink satellites doing irreparable damage to the night sky

---

In summary the space-billionaire future is a _radical_ ideology of acceleration in the time where all attention should be on planetary limits. At its worse, space-faring is a dark exit stategy, a deep betrayal of life on earth, as it effectively gives up on it and seeks the next planet to pillage. 

Not only is this promise flawed but entrepreneurs also continually fail to deliver it, as illustrated by the 2017 timeline that shows flights for Mars flights taking place by 2023 (Musk, 2017), the frontier is always 5 years away for Elon Musk.

Rubenstein (2022) demonstrates how the ideological foundations of space colonialism  are entangled with religion.
As we feel the urgency to protect ourselves and the planet from this deadly  future, pushing back on a spiritual level seems like a relevant terrain. 

---

Perhaps turning to magic or the occult is also one of the last few options to reclaim a sense of agency as so little else seems available to counter the current direction of travel.

---

Chaos Magic is a modern tradition of magic caracterized by the use of paradigm shifting, free cosmogony and belief as a tool to achieve effects. Famous figures include turn of the 20th century occultist and philospher Alister Crowley and one of his key sources of inspiration, artist Austin Osman Spare. The latter revived sigils as 'the art of believing' in a 1913 publication, (Spare, 1913, p.86) a way to mobilise one's unconscious power towards the manifestation of a plan (p.100). 

---

Ciphers are a much older magical tradition, according to Mark B. Jackson (2013), they organise letters spatially to allow for the encryption of sentences or words.
We chose to work with a 3 by 3 square cipher grid, somewhere between a magical square and the phone keyboard cipher dear to elderly millenials

---

We started experimenting with sigils back in 2021 with the first generator to encode the phrase: "Bind billionaires who fly to the upper sky from coming back to Earth". 

---

With the Sigil Séance we are adding a collaborative element to this sigil making, allowing for a group of people to gather on a web page to generate and launch sigils together. The application is built with Yjs, a library normally used for collaborative text editing (Nicolaescu et al. 2016, Jahns 2014).

As they connect, each participant is given a user-name and an emoji avatar that appear in the presence list on the page and on the cipher grid.
Anyone can edit or replace the phrase in the main text box and click a button to draw the sigil and sync it to all other participants. 

Ritual paticipants are all connected through an audio call. 

One participant types a phrase to launch and invites edits or additions from the others. The software converts each phrase to a sigil by 1) condensing all letters and removing duplicates such that each letter cannot appear again after its first position, and 2) tracing the resulting sequence of letters on the cypher grid (Jackson, 2013). 

All participants must express consent before proceeding to the sigil launch. Consent is reset each time the sigil is changed to ensure that the collective energy cannot be subverted at the last minute.

Participants send and receive letters according to their position on the grid.
This process repeats until the whole letter sequence is completed, at which point the sigil is considered launched and one participant clears the grid by sending an empty update.

This launch procedure is repeated a number of times during the séance ritual which we designed as a teleperformance

---

Ritual is, according to Richard Schechner (2002), one of foundational element of performance. In fact performance can be defined simply as ritualised gestures and sounds. Performance practictioners have shown conflicting relationships to technology, and the mediation it operates between performers, participants, and audience.

We follow the late legacy of the Cyborg Manifesto and embrace its impure ontology that affects the subject and, in our ritual, the performing subjects. Therefore, we characterise our project as a *teleperformance* (Haute, 2010) with an implicit use of technology. The traditional ouija board or séance arrangement is replaced by a web page within which we collectively charge and launch the sigil of a spell.

---

In the field of political activism, collective action is a strategy adopted to resist control mechanisms based on individualization. 
Our work is an opportunity to revisit past examples of groups and movements mobilising magic as a means of political action.

---

In 1970's Japan, a group of monks called Jusatsu Kito Sodan were part of the vanguard of the country's environmental movement. Their name translates to 'Killing Curse Prayer Monk Group' and they were documented by photographer Mitsutoshi Hanaga as they traveled to sites of industrial polution to curse the factory owners responsible, through the repeating of mantras and other ritual practices (Hopfner, 2020).

---

The Women’s International Terrorist Conspiracy from Hell (WITCH) was a feminist group active in the United States during the 1960s women’s liberation movement. Their first action consisted in a march down Wall Street, to hex New York’s financial district. 
The anonymous collective re-formed in the wake of Donald Trump’s 2016 election, first in Portland and then in many other American cities. The 2016 WITCH explicitly reference to their 1960s predecessors, quoting them on in an Instagram post.

---

Simultaneously, on Instagram and Twitter a cyber-magic resistance is being organized.
At each full moon, a ritual rendezvous is performed individually and remotely become collective through the hashtags #BindTrump and #magicresistance. The revival of WITCH in the social network era has mobilized supporters of both sides. In response, Pro-Trump evangelical nationalists gathered through the hashtag #PrayerResistance 

---

Finally, although not really magic, we'd like to mention [_The Hologram_](https://thehologram.xyz) by Cassie Thornton (2020). As a collective response to catastrophic failures of healthcare systems, Thornton proposed a peer-to-peer model where participants assemble in a formation around the individual to care for, reclaiming some of the care work that is denied to them. 

---

While this project started innocently with a sigil generator, coordinating the ritual activation of sigils by mutiple people through a collaborative application forces us to take the potential consequences more seriously.

---

The Jusatsu Kito Sodan monks mentioned earlier deployed _abhicara_, or black magic, which would be frowned upon by many buddhist who adhere to strict non-violence. According to the group however the amount of deadly damage unleashed by industrial polution were of such a magnitude that a kill curse was warranted.

In our case we also consider that 'the entire cosmic order' is threatened enough to warrant serious curses. 

However, we are also mindful that our application is aimed at inviting others to consider their own level of proportionate response.

We designed the consent mechanism explained above to provide a fail-safe in the design. We also added a disclaimer to our invitation text and website to make this aspect clear to participants

---

This paper is the first we share about this project so it feels like a starting point. We have identified an area for a kind of praxis between analysing the radicalised future of space-billinaires and providing a space to express our resistance to it in cooperation with others.

We led a succesful inaugural session with 4 participants that all had positive feedback and have commited to attending the next session when it happens. With this budding community of practice, the next step will be to try to give the séances a recurring temporality, perhaps aligned with that of commercial space launches. 

There are a number of technical developments that would be interesting, such as opening up more "rooms" or pages for people to have sigil rituals without our facilitation. We are interested in infrastructural politics and the poetics of peer-to-peer networks so we will experiment with protocols such as WebRTC where people's devices are connected directly (after being introduced by a signaling server).

---

finally On a theoretical level, we want to engage more with alternative visions of space and the cosmos. In particular we want to engage more with indigenous visions that have been resisting colonial and ecocidal futures for hundreds of years. In the words of Mary-Jane Rubenstein, we want to seek out better mythologies.

---

many thanks for your attention

- you can test the application at this link
- we welcome feedback at the email sigil-seance@diagram.institute
- also email if you would like to be notified at the next seance



- Acknowledgements
  - participants in the inaugural session 
  - Aaron MacSween for sharing his expert knowledge on peer-to-peer protocols

:::
